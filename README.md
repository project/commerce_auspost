Drupal Commerce
===============

Integrates Australia Post postage estimation with Drupal Commerce on Drupal 10+.

## Requirements

* [An Australia Post PAC API key](https://developers.auspost.com.au/apis/pacpcs-registration)
* Drupal 10.3+
* Drupal Commerce
* Commerce Shipping

## Installation

Use [Composer](https://getcomposer.org/) to get Commerce AusPost and all of its
dependencies installed on your Drupal site, installing via a tarball from drupal.org **is not supported**. The
installation instructions can be found on [Project page](https://www.drupal.org/project/commerce_auspost)

* Then simply enable the "AusPost (Commerce Shipping)" module and visit
`Commerce > Configuration > Shipping Methods` to configure the shipping method.

Do note, the more shipping services you have enabled, the slower postage calculations will be.

**There is currently no upgrade path between different major versions of this module.**

## Configuration
* API settings and desired shipping services can be configured on the shipping methods page (`Commerce > Configuration > Shipping Methods`)
* Custom package types can be added on the package types (`Commerce > Configuration > Package Types`)
